package com.example.ldap.service;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;


@Service
public class LdapUserServiceImpl implements LdapUserService {
	 @Autowired
	    protected ContextSource contextSource;
	    @Autowired
	    protected LdapTemplate ldapTemplate;
	   @Override
	    public boolean authenticate(String userDn, String credentials) throws UnsupportedEncodingException {
		  final byte[] md5 = DigestUtils.md5(credentials.trim().getBytes("UTF-8"));
		  final byte[] base64 = Base64.encodeBase64(md5);
		  final String hashedAndEncoded = new String(base64, "ASCII");
		  final String userPwd="{MD5}" + hashedAndEncoded;
	        AndFilter filter = new AndFilter();
	        filter.and(new EqualsFilter("uid", userDn));
			 filter.and(new EqualsFilter("userPassword", userPwd)); 

	        return ldapTemplate.authenticate("dc=example,dc=com",filter.toString(), credentials);
	    }
		

}
