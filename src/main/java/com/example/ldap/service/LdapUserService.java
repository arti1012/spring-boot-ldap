package com.example.ldap.service;

import java.io.UnsupportedEncodingException;

public interface LdapUserService {

	boolean authenticate(String userDn, String credentials) throws UnsupportedEncodingException;

}
