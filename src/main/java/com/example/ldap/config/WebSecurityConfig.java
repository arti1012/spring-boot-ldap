package com.example.ldap.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.LdapShaPasswordEncoder;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.search.LdapUserSearch;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter  {
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().exceptionHandling()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html",
                        "/**/*.css", "/**/*.js")
                .permitAll().antMatchers(HttpMethod.POST, "/signin/**").permitAll()
				.anyRequest()
				.authenticated().and().formLogin();
	}
	/*
	 * @Override public void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth .ldapAuthentication() .userSearchFilter("(uid={0})")
	 * .userSearchBase("dc=example,dc=com") .groupSearchFilter("uniqueMember={0}")
	 * .groupSearchBase("ou=people,dc=example,dc=com") .userDnPatterns("uid={0}")
	 * .contextSource() .url("ldap://localhost:10389")
	 * .managerDn("uid=admin,ou=system") .managerPassword("admin@1234"); }
	 */
	
	  @Override public void configure(AuthenticationManagerBuilder auth) throws
	  Exception { auth.ldapAuthentication().userDnPatterns("uid={0},people")
	  .groupSearchBase("ou=groups") .contextSource()
	  .url("ldap://localhost:10389/dc=example,dc=com").and().passwordCompare()
	  .passwordAttribute("userPassword"); }
	 
	
		/*
		 * @Bean public BaseLdapPathContextSource contextSource() { LdapContextSource
		 * bean = new LdapContextSource();
		 * bean.setUrl("ldap://localhost:8389/dc=springframework,dc=org");
		 * bean.setBase("DC=SpringFramework,DC=org"); //instead of this i want to put
		 * here the username and password provided by the user
		 * bean.setUserDn("SpringFramework\\uid{0}"); bean.setPassword("userPassword");
		 * bean.setPooled(true); bean.setReferral("follow"); bean.afterPropertiesSet();
		 * return bean; }
		 */
	 


	/*
	 * @Bean public LdapTemplate ldapTemplate() { LdapTemplate template = new
	 * LdapTemplate(contextSource());
	 * 
	 * return template; }
	 */
}
