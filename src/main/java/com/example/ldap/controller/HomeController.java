package com.example.ldap.controller;


import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ldap.ApiResponse;
import com.example.ldap.model.Login;
import com.example.ldap.service.LdapUserService;

@CrossOrigin(origins="http://localhost:3000")
@RestController
public class HomeController {
	@Autowired
	  LdapUserService ldapUserService;
	@PostMapping("/signin")
	public ResponseEntity<?> signin(@RequestBody Login login) {
		
		
		System.out.println("User");
		System.out.println(login.getUserName() + login.getPassword());
		boolean isAuthenticated =false;
		try {
			isAuthenticated = ldapUserService.authenticate(login.getUserName(),login.getPassword());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!isAuthenticated) {
		return new ResponseEntity(new ApiResponse(false,"Authenticated failed"),HttpStatus.BAD_REQUEST);
	}
		return new ResponseEntity(new ApiResponse(true,"Authenticated successful"),HttpStatus.OK);
}
	@GetMapping("/api")
	public String index() {
		return "Welcome to Home Page";
	}

}
