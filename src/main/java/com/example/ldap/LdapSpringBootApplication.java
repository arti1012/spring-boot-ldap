package com.example.ldap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LdapSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(LdapSpringBootApplication.class, args);
	}

}
